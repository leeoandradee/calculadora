package br.com.calculadora.utils;

import br.com.calculadora.models.Calculadora;

public class Utils {

    public static boolean temNumeroNegativo(Calculadora calculadora) {
        for (Integer numero: calculadora.getNumeros()) {
            if (numero < 0) {
                return true;
            }
        }
        return false;
    }


}
