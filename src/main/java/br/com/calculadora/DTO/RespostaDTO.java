package br.com.calculadora.DTO;

public class RespostaDTO {

    private int resposta;

    public RespostaDTO(int resposta) {
        this.resposta = resposta;
    }

    public int getResposta() {
        return resposta;
    }

    public void setResposta(int resposta) {
        this.resposta = resposta;
    }
}
