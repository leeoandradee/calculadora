package br.com.calculadora.services;

import br.com.calculadora.DTO.RespostaDTO;
import br.com.calculadora.models.Calculadora;
import org.springframework.stereotype.Service;

@Service
public class CalculadoraService {

    public RespostaDTO somar(Calculadora calculadora) {
        int resultado = 0;
        for (Integer numero: calculadora.getNumeros()) {
            resultado = resultado + numero;
        }
        return new RespostaDTO(resultado);
    }

    public RespostaDTO subtrair(Calculadora calculadora) {
        int resultado = 0;
        for (Integer numero: calculadora.getNumeros()) {
            resultado = resultado - numero;
        }
        return new RespostaDTO(resultado);
    }

    public RespostaDTO multiplicar(Calculadora calculadora) {
        int resultado = 0;
        for (Integer numero: calculadora.getNumeros()) {
            if(resultado == 0) {
                resultado = numero;
            } else {
                resultado = resultado * numero;
            }
        }
        return new RespostaDTO(resultado);
    }

    public RespostaDTO dividir(Calculadora calculadora) {
        int resultado = 0;
        for (Integer numero: calculadora.getNumeros()) {
            resultado = resultado / numero;
        }
        return new RespostaDTO(resultado);
    }

}
