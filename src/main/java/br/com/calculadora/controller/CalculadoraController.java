package br.com.calculadora.controller;

import br.com.calculadora.DTO.RespostaDTO;
import br.com.calculadora.models.Calculadora;
import br.com.calculadora.services.CalculadoraService;
import br.com.calculadora.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/calculadora")
public class CalculadoraController {

    @Autowired
    private CalculadoraService calculadoraService;


    @PostMapping("/somar")
    public RespostaDTO somar(@RequestBody Calculadora calculadora) {
        if (calculadora.getNumeros().size() < 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário ter algum número na lista");
        }
        return calculadoraService.somar(calculadora);
    }

    @PostMapping("/subtrair")
    public RespostaDTO subtrair(@RequestBody Calculadora calculadora) {
        if (calculadora.getNumeros().size() < 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário ter algum número na lista");
        } else if (calculadora.getNumeros().size() == 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário ter somente 2 números");
        }
        return calculadoraService.subtrair(calculadora);
    }

    @PostMapping("/multiplicar")
    public RespostaDTO multiplicar(@RequestBody Calculadora calculadora) {
        if (calculadora.getNumeros().size() < 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário ter algum número na lista");
        } else if (Utils.temNumeroNegativo(calculadora)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário ter números naturais");
        }
        return calculadoraService.multiplicar(calculadora);
    }

    @PostMapping("/dividir")
    public RespostaDTO dividir(@RequestBody Calculadora calculadora) {
        if (calculadora.getNumeros().size() < 0) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário ter algum número na lista");
        } else if (Utils.temNumeroNegativo(calculadora)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário ter números naturais");
        } else if (calculadora.getNumeros().size() < 2 || calculadora.getNumeros().size() > 2) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "É necessário ter somente dois números");
        }
        return calculadoraService.dividir(calculadora);
    }


}
